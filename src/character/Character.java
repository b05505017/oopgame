package character;

public class Character {
	public boolean visible=false;
	private String type;
	private int level;
	private int xcoor;
	private int ycoor;
	public String team;
	public String gettype() {
		return type;
	}
	public boolean getvisible() {
		return visible;
	}
	public void settype(String t) {
		type = t;
		if (type.equalsIgnoreCase("A")) level=7;
		if (type.equalsIgnoreCase("B")) level=6;
		if (type.equalsIgnoreCase("C")) level=5;
		if (type.equalsIgnoreCase("D")) level=4;
		if (type.equalsIgnoreCase("E")) level=3;
		if (type.equalsIgnoreCase("F")) level=2;
		if (type.equalsIgnoreCase("G")) level=1;
	}
	public void setx(int x) {
		xcoor = x;
	}
	public void sety(int y) {
		ycoor = y;
	}
	public int getx() {return xcoor;}
	public int gety() {return ycoor;}
	
	public void setteam(String te) {
		team = te;
	}
	public String getteam() {return team;}
	
	/**
	 *  Returns level of the character.
	 *  (level is used to check is the killing action is valid.)
	 */
	public int getlevel() 
	{return level;}
	
	/**	
	 *  If the chess is not flipped, this function is called to flip the chess.
	 */
	public void flip() {
		visible = true;
	}

}
