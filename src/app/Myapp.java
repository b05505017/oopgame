package app;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import character.Character;
import map.Map;
import map.Element;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import javax.swing.JScrollPane;

public class Myapp {
	private static Map map = new Map();
	static int redteam = 16;
	static int blackteam = 16;
	private JFrame frame = new JFrame();
	private static JTextArea textArea = new JTextArea();
	private static int round = 1;
	public static Font font = new Font("Microsoft JhengHei UI", Font.TRUETYPE_FONT, 20);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Myapp window = new Myapp();
					window.frame.setVisible(true);
					window.frame.getContentPane().setBackground(new Color(0, 0, 0));
					window.frame.setBounds(100, 100, 617, 455);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public Myapp() {
		textArea.setEditable(false);
		textArea.setBounds(9, 10, 400, 101);
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		round = 1;
		redteam = 16;
		blackteam = 16;
		textArea.setText(null);
		frame.getContentPane().removeAll();
		frame.getContentPane().revalidate();
		frame.getContentPane().repaint();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton restart = new JButton("Restart");
		restart.setFont(font);
		restart.setBounds(490, 30, 100, 60);
		frame.getContentPane().add(restart);

		restart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initialize();
			}
		});

		JScrollPane scrollpane = new JScrollPane(textArea);
		scrollpane.setSize(470, 101);
		scrollpane.setLocation(9, 10);
		frame.getContentPane().add(scrollpane);

		int t = 9;
		Element[][] e = new Element[8][4];
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 8; x++) {
				e[x][y] = new Element();

				if (y == 0)
					e[x][y].setBounds(t, 343, 68, 68);
				if (y == 1)
					e[x][y].setBounds(t, 269, 68, 68);
				if (y == 2)
					e[x][y].setBounds(t, 195, 68, 68);
				if (y == 3)
					e[x][y].setBounds(t, 121, 68, 68);
				t = t + 74;
				if (x == 7)
					t = 9;
				frame.getContentPane().add(e[x][y]);
			}
		}
		map.setmap(e);

		if (round == 1) {
			textArea.setText("Round 1. (Red team's turn). Please flip a chess.\r\n");
		}

		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 4; y++) {
				int a = x;
				int b = y;
				map.getelement(x, y).addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (map.getelement(a, b).getcharacter().getvisible() == false) {
							map.getelement(a, b).getcharacter().flip();
							round++;
							map.getelement(a, b)
									.setIcon(new ImageIcon(util.geticon(map.getelement(a, b).getcharacter().getteam(),
											map.getelement(a, b).getcharacter().gettype())));
							if (round % 2 == 1)
								textArea.setText(textArea.getText() + "Round " + round
										+ " (Red team's turn). Please flip a chess or select a character.\r\n");
							if (round % 2 == 0)
								textArea.setText(textArea.getText() + "Round " + round
										+ " (Black team's turn). Please flip a chess or select a character.\r\n");
						} else if (map.getelement(a, b).getflag() && round % 2 == 1 && map.getelement(a, b).getcharacter().getteam().equals("Red")) {
							textArea.setText(textArea.getText() + "Press Up, Down, Left or Right to control.\r\n");
						} else if (map.getelement(a, b).getflag() && round % 2 == 0 && map.getelement(a, b).getcharacter().getteam().equals("Black")) {
							textArea.setText(textArea.getText() + "Press Up, Down, Left or Right to control.\r\n");
						} else if (!map.getelement(a, b).getflag()) {
							textArea.setText(textArea.getText() + "Nothing there\r\n");
						} else {
							textArea.setText(textArea.getText() + "You can not control your rival's character.\r\n");
						}

					}
				});
			}
		}
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 4; y++) {
				int a = x;
				int b = y;
				map.getelement(x, y).addKeyListener(new KeyAdapter() {
					public void keyPressed(KeyEvent e) {
						if ((round % 2 == 1 && map.getelement(a, b).getcharacter().getteam().equals("Red"))
								|| (round % 2 == 0 && map.getelement(a, b).getcharacter().getteam().equals("Black"))) {
							if (map.getelement(a, b).getflag() == true) {
								// System.out.println(e.getKeyCode());
								process(e.getKeyCode(), map.getelement(a, b).getcharacter(), map);

							} else
								textArea.setText(textArea.getText() + "Nothing there.\r\n");
						}
					}
				});
			}
		}

	}
	
	
	/**
	 * 
	 * This function determines the action when a button is pressed:
	 * If the chess is not flipped, flip the chess.
	 * If the chess is flipped, it asks whether which direction the player 
	 * would like to move his/her chess.  
	 * The player enters up, down, left, right to determine the direction. 
	 * 
	 */
	public void process(int direction, Character me, Map mymap) {
		Character rival = new Character();

		switch (direction) {
		// LEFT
		case 37:
			if (me.getx() > 0) {
				if (me.gettype().equalsIgnoreCase("F")) {
					Character cannonrival = util.findcannonrival(me, "LEFT", mymap);

					if (map.getelement(me.getx() - 1, me.gety()).getflag() && cannonrival != null) {
						// textArea.setText(textArea.getText()+"Find a rival at "+cannonrival.getx()+" ,
						// "+cannonrival.gety()+"\r\n");
						util.kill(me, cannonrival, map);

					} else if (!map.getelement(me.getx() - 1, me.gety()).getflag() && cannonrival == null) {
						util.move(me, "LEFT", map);

					} else if (map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival == null) {
						textArea.setText(textArea.getText() + "Invalid action\r\n");
					} else {
						util.choiceforcannon(me, cannonrival, "LEFT", map);
					}
				} else {
					if (!map.getelement(me.getx() - 1, me.gety()).getflag()) {

						util.move(me, "LEFT", map);
					} else {

						rival = map.getelement(me.getx() - 1, me.gety()).getcharacter();
						util.kill(me, rival, map);
						util.move(me, "LEFT", map);
					}
				}
				if (round % 2 == 1)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Red team's turn). Please flip a chess or select a character.\r\n");
				if (round % 2 == 0)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Black team's turn). Please flip a chess or select a character.\r\n");
			} else {
				textArea.setText(textArea.getText() + "Invalid action\r\n");
			}
			break;
		// RIGHT
		case 39:
			if (me.getx() < 7) {
				if (me.gettype().equalsIgnoreCase("F")) {
					Character cannonrival = util.findcannonrival(me, "RIGHT", mymap);

					if (map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival != null) {
						// textArea.setText(textArea.getText()+"Find a rival at "+cannonrival.getx()+" ,
						// "+cannonrival.gety());
						util.kill(me, cannonrival, map);
					} else if (!map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival == null) {
						util.move(me, "RIGHT", map);
					} else if (map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival == null) {
						textArea.setText(textArea.getText() + "Invalid action\r\n");
					} else {
						util.choiceforcannon(me, cannonrival, "RIGHT", map);
					}
				} else {
					if (!map.getelement(me.getx() + 1, me.gety()).getflag()) {
						util.move(me, "RIGHT", map);
					} else {
						rival = map.getelement(me.getx() + 1, me.gety()).getcharacter();
						util.kill(me, rival, map);
						util.move(me, "RIGHT", map);
					}
				}
				if (round % 2 == 1)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Red team's turn). Please flip a chess or select a character.\r\n");
				if (round % 2 == 0)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Black team's turn). Please flip a chess or select a character.\r\n");
			} else {
				textArea.setText(textArea.getText() + "Invalid action\r\n");
			}
			break;
		// UP
		case 38:
			if (me.gety() < 3) {
				if (me.gettype().equalsIgnoreCase("F")) {
					Character cannonrival = util.findcannonrival(me, "UP", mymap);
					if (map.getelement(me.getx(), me.gety() + 1).getflag() && cannonrival != null) {
						// textArea.setText(textArea.getText()+"Find a rival at "+cannonrival.getx()+" ,
						// "+cannonrival.gety()+"\r\n");
						util.kill(me, cannonrival, map);
					} else if (!map.getelement(me.getx(), me.gety() + 1).getflag() && cannonrival == null) {
						util.move(me, "UP", map);
					} else if (map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival == null) {
						textArea.setText(textArea.getText() + "Invalid action\r\n");
					} else {
						util.choiceforcannon(me, cannonrival, "UP", map);
					}
				} else {
					if (!map.getelement(me.getx(), me.gety() + 1).getflag()) {
						util.move(me, "UP", map);
					} else {
						rival = map.getelement(me.getx(), me.gety() + 1).getcharacter();
						util.kill(me, rival, map);
						util.move(me, "UP", map);
					}
				}
				if (round % 2 == 1)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Red team's turn). Please flip a chess or select a character.\r\n");
				if (round % 2 == 0)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Black team's turn). Please flip a chess or select a character.\r\n");
			} else {
				textArea.setText(textArea.getText() + "Invalid action\r\n");
			}
			break;
		// DOWN
		case 40:
			if (me.gety() > 0) {
				if (me.gettype().equalsIgnoreCase("F")) {
					Character cannonrival = util.findcannonrival(me, "DOWN", mymap);
					if (map.getelement(me.getx(), me.gety() - 1).getflag() && cannonrival != null) {
						// textArea.setText(textArea.getText()+"Find a rival at "+cannonrival.getx()+" ,
						// "+cannonrival.gety());
						util.kill(me, cannonrival, map);
					} else if (!map.getelement(me.getx(), me.gety() - 1).getflag() && cannonrival == null) {
						util.move(me, "DOWN", map);
					} else if (map.getelement(me.getx() + 1, me.gety()).getflag() && cannonrival == null) {
						textArea.setText(textArea.getText() + "Invalid action\r\n");
					} else {
						util.choiceforcannon(me, cannonrival, "DOWN", map);
					}
				} else {
					if (!map.getelement(me.getx(), me.gety() - 1).getflag()) {
						util.move(me, "DOWN", map);
					} else {
						rival = map.getelement(me.getx(), me.gety() - 1).getcharacter();
						util.kill(me, rival, map);
						util.move(me, "DOWN", map);
					}
				}
				if (round % 2 == 1)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Red team's turn). Please flip a chess or select a character.\r\n");
				if (round % 2 == 0)
					textArea.setText(textArea.getText() + "Round " + round
							+ " (Black team's turn). Please flip a chess or select a character.\r\n");
			} else {
				textArea.setText(textArea.getText() + "Invalid action\r\n");
			}
			break;
		default:
			break;
		}
	}
	
	/**
	 * This function outputs a window that tells the winner.
	 */
	public static void winner() {
		
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 4; y++) {

				map.getelement(x, y).removeActionListener(map.getelement(x, y).getActionListeners()[0]);

			}
		}
		
		JFrame choice = new JFrame();
		choice.setBounds(270, 220, 300, 180);
		choice.getContentPane().setBackground(new Color(255,255,255));
		choice.getContentPane().setLayout(null);
		choice.setVisible(true);
		choice.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		JTextField notew = new JTextField();
		notew.setBorder(null);
		notew.setBackground(new Color(255,255,255));
		notew.setHorizontalAlignment(JTextField.CENTER);
		notew.setEditable(false);
		notew.setBounds(0, 10, 285, 35);
		notew.setFont(font);
		if (redteam == 0)
			notew.setText("BLACK TEAM IS THE WINNER!");
		if (blackteam == 0)
			notew.setText("RED TEAM IS THE WINNER!");
		choice.getContentPane().add(notew);
		notew.setFont(new Font("Microsoft JhengHei UI", Font.TRUETYPE_FONT, 18));
		JButton button1 = new JButton("Close");
		button1.setFont(new Font("Microsoft JhengHei UI", Font.TRUETYPE_FONT, 20));
		
		button1.setBounds(95, 68, 100, 60);
		choice.getContentPane().add(button1);

		button1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				choice.dispose();
			}
		});

	}

	
	/**
	 * 
	 * This class has these functions:
	 * ran (int min, int max) : generates a random number between the minimum and maximum nunmber.
	 * move(Character me, String direction, Map mymap) : move the character
	 * kill(Character me, Character rival, Map mymap) : kill the rival
	 * 
	 * 
	 * 
	 */
	public static class util {

		/**
		 * 
		 * This function return a random number between minimum and maximum number.
		 * 
		 * @param min
		 * @param max
		 * @return a random 
		 */
		public static int ran(int min, int max) {
			int rnd;
			rnd = (int) (Math.random() * (max - min + 1)) + min;
			return rnd;
		}

		public static Image geticon(String te, String type) {
			Image img = null;
			try {
				switch (te) {
				case "Red":
					switch (type) {
					case "A":
						img = ImageIO.read(new FileInputStream("img/red_7.png"));
						break;
					case "B":
						img = ImageIO.read(new FileInputStream("img/red_6.png"));
						break;
					case "C":
						img = ImageIO.read(new FileInputStream("img/red_5.png"));
						break;
					case "D":
						img = ImageIO.read(new FileInputStream("img/red_4.png"));
						break;
					case "E":
						img = ImageIO.read(new FileInputStream("img/red_3.png"));
						break;
					case "F":
						img = ImageIO.read(new FileInputStream("img/red_2.png"));
						break;
					case "G":
						img = ImageIO.read(new FileInputStream("img/red_1.png"));
						break;
					}
					break;
				case "Black":
					switch (type) {
					case "a":
						img = ImageIO.read(new FileInputStream("img/black_7.png"));
						break;
					case "b":
						img = ImageIO.read(new FileInputStream("img/black_6.png"));
						break;
					case "c":
						img = ImageIO.read(new FileInputStream("img/black_5.png"));
						break;
					case "d":
						img = ImageIO.read(new FileInputStream("img/black_4.png"));
						break;
					case "e":
						img = ImageIO.read(new FileInputStream("img/black_3.png"));
						break;
					case "f":
						img = ImageIO.read(new FileInputStream("img/black_2.png"));
						break;
					case "g":
						img = ImageIO.read(new FileInputStream("img/black_1.png"));
						break;
					}
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return img;
		}
		
		/**
		 * 
		 * This function moves the character.
		 * @param me
		 * @param direction
		 * @param mymap
		 */
		public static void move(Character me, String direction, Map mymap) {
			if (me.getvisible()) {

				switch (direction) {
				case "LEFT":
					if (!mymap.getelement(me.getx() - 1, me.gety()).getflag()) {
						mymap.getelement(me.getx(), me.gety()).resetflag();
						mymap.getelement(me.getx() - 1, me.gety()).setflag(me);
						me.setx(me.getx() - 1);
						mymap.getelement(me.getx(), me.gety()).seticon();
						round++;
					} else {
						textArea.setText(textArea.getText() + "Invalid action.\r\n");
					}
					break;
				case "RIGHT":
					if (!mymap.getelement(me.getx() + 1, me.gety()).getflag()) {
						mymap.getelement(me.getx(), me.gety()).resetflag();
						mymap.getelement(me.getx() + 1, me.gety()).setflag(me);
						me.setx(me.getx() + 1);
						mymap.getelement(me.getx(), me.gety()).seticon();
						round++;
					} else {
						textArea.setText(textArea.getText() + "Invalid action.\r\n");
					}
					break;
				case "UP":
					if (!mymap.getelement(me.getx(), me.gety() + 1).getflag()) {
						mymap.getelement(me.getx(), me.gety()).resetflag();
						mymap.getelement(me.getx(), me.gety() + 1).setflag(me);
						me.sety(me.gety() + 1);
						mymap.getelement(me.getx(), me.gety()).seticon();
						round++;
					} else {
						textArea.setText(textArea.getText() + "Invalid action.\r\n");
					}
					break;
				case "DOWN":
					if (!mymap.getelement(me.getx(), me.gety() - 1).getflag()) {
						mymap.getelement(me.getx(), me.gety()).resetflag();
						mymap.getelement(me.getx(), me.gety() - 1).setflag(me);
						me.sety(me.gety() - 1);
						mymap.getelement(me.getx(), me.gety()).seticon();
						round++;
					} else {
						textArea.setText(textArea.getText() + "Invalid action.\r\n");
					}
					break;
				}
			}
		}
		
		/**
		 * This function kills rival's chess
		 * @param me
		 * @param rival
		 * @param mymap
		 */
		public static void kill(Character me, Character rival, Map mymap) {
			// if my character is cannon
			if (me.getlevel() == 2) {
				if (validactionforcannon(me, rival, mymap)) {
					if (rival.getteam().equals("Red"))
						redteam--;
					if (rival.getteam().equals("Black"))
						blackteam--;
					mymap.getelement(me.getx(), me.gety()).resetflag();
					mymap.getelement(rival.getx(), rival.gety()).resetflag();
					me.setx(rival.getx());
					me.sety(rival.gety());
					mymap.getelement(rival.getx(), rival.gety()).setflag(me);
					mymap.getelement(me.getx(), me.gety()).seticon();
					round++;

				} else {
					textArea.setText(textArea.getText() + "Invalid action for a cannon.\r\n");
				}
			} else if (me.getvisible() && rival.getvisible() && !me.getteam().equals(rival.getteam())) {

				// if my character is soldier
				if (me.getlevel() == 1) {
					if (rival.getlevel() == 7 || rival.getlevel() == 1) {
						if (rival.getteam().equals("Red"))
							redteam--;
						if (rival.getteam().equals("Black"))
							blackteam--;
						mymap.getelement(rival.getx(), rival.gety()).resetflag();
					}
				}
				// if my character is the general
				else if (me.getlevel() == 7) {
					if (rival.getlevel() != 1) {
						if (rival.getteam().equals("Red"))
							redteam--;
						if (rival.getteam().equals("Black"))
							blackteam--;
						mymap.getelement(rival.getx(), rival.gety()).resetflag();
					}

				}
				// other cases
				else {
					if (me.getlevel() >= rival.getlevel()) {
						if (rival.getteam().equals("Red"))
							redteam--;
						if (rival.getteam().equals("Black"))
							blackteam--;
						mymap.getelement(rival.getx(), rival.gety()).resetflag();
					}
				}
			} else {
				// textArea.setText(textArea.getText()+"Invalid action.\r\n");
			}
			if (redteam == 0 || blackteam == 0) {
				winner();
			}
		}

		/**
		 * choiceforcannon : if the selected chess is a cannon and it can both eat or kill , 
		 * 
		 */
				
		public static void choiceforcannon(Character me, Character rival, String direction, Map map) {

			JFrame choice = new JFrame();
			choice.setBounds(500, 100, 267, 130);
			choice.getContentPane().setLayout(null);
			choice.setVisible(true);

			JButton button1 = new JButton("Kill");
			button1.setFont(font);

			button1.setBounds(20, 18, 100, 60);
			choice.getContentPane().add(button1);

			JButton button2 = new JButton("Move");
			button2.setFont(font);
			button2.setBounds(130, 18, 100, 60);
			choice.getContentPane().add(button2);

			button1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					util.kill(me, rival, map);
					if (round % 2 == 1)
						textArea.setText(textArea.getText() + "Round " + round
								+ " (Red team's turn). Please flip a chess or select a character.\r\n");
					if (round % 2 == 0)
						textArea.setText(textArea.getText() + "Round " + round
								+ " (Black team's turn). Please flip a chess or select a character.\r\n");
					choice.dispose();
				}
			});

			button2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					util.move(me, direction, map);
					if (round % 2 == 1)
						textArea.setText(textArea.getText() + "Round " + round
								+ " (Red team's turn). Please flip a chess or select a character.\r\n");
					if (round % 2 == 0)
						textArea.setText(textArea.getText() + "Round " + round
								+ " (Black team's turn). Please flip a chess or select a character.\r\n");
					choice.dispose();
				}
			});
		}

		/**
		 * This function checks whether my cannon and the target rival is separated by
		 * one chess
		 */
		private static boolean validactionforcannon(Character me, Character rival, Map mymap) {
			int count = 0;
			int distance = 0;
			// System.out.println(me.getx()+" "+me.gety());
			// System.out.println(rival.getx()+" "+rival.gety());
			if (me.getx() == rival.getx() && me.gety() != rival.gety()) {
				if (me.gety() > rival.gety()) {
					distance = me.gety() - rival.gety();
					for (int i = 1; i < distance; i++) {
						if (mymap.getelement(me.getx(), me.gety() - i).getflag()) {
							count++;
						}
					}
				} else if (me.gety() < rival.gety()) {
					distance = rival.gety() - me.gety();
					for (int i = 1; i < distance; i++) {
						if (mymap.getelement(me.getx(), me.gety() + i).getflag()) {
							count++;
						}
					}
				}
				// System.out.println(count);
				if (count == 1)
					return true;
				else
					return false;
			} else if (me.getx() != rival.getx() && me.gety() == rival.gety()) {
				if (me.getx() > rival.getx()) {
					distance = me.getx() - rival.getx();
					for (int i = 1; i < distance; i++) {
						if (mymap.getelement(me.getx() - i, me.gety()).getflag()) {
							count++;
						}
					}
				} else if (me.getx() < rival.getx()) {
					distance = rival.getx() - me.getx();
					for (int i = 1; i < distance; i++) {
						if (mymap.getelement(me.getx() + i, me.gety()).getflag()) {
							count++;
						}
					}
				}
				// System.out.println(count);
				if (count == 1)
					return true;
				else
					return false;
			} else
				textArea.setText(textArea.getText() + "Nothing has done will return false " + count + "\r\n");
			return false;

		}

		public static Character findcannonrival(Character me, String direction, Map mymap) {
			Character rival = null;
			Character next = null;
			int x = me.getx();
			int y = me.gety();
			int i = 1;
			switch (direction) {
			case "RIGHT":
				while (x + i <= 7) {
					if (mymap.getelement(x + i, y).getflag()) {
						next = mymap.getelement(x + i, y).getcharacter();
						break;
					} else
						i++;
				}
				i = 1;
				if (next != null) {
					while (next.getx() + i <= 7) {
						if (mymap.getelement(next.getx() + i, y).getflag()) {
							rival = mymap.getelement(next.getx() + i, y).getcharacter();
							if (me.getteam().equals(rival.getteam())) {
								rival = null;
								break;
							} else {
								break;
							}
						} else
							i++;
					}
				}

				break;
			case "LEFT":
				while (x - i >= 0) {
					if (mymap.getelement(x - i, y).getflag()) {
						next = mymap.getelement(x - i, y).getcharacter();
						break;
					} else
						i++;
				}
				i = 1;
				if (next != null) {
					while (next.getx() - i >= 0) {
						if (mymap.getelement(next.getx() - i, y).getflag()) {
							rival = mymap.getelement(next.getx() - i, y).getcharacter();
							if (me.getteam().equals(rival.getteam())) {
								rival = null;
								break;
							} else {
								break;
							}
						} else
							i++;
					}
				}
				break;
			case "UP":
				while (y + i <= 3) {
					if (mymap.getelement(x, y + i).getflag()) {
						next = mymap.getelement(x, y + i).getcharacter();
						break;
					} else
						i++;
				}
				i = 1;
				if (next != null) {
					while (next.gety() + i <= 3) {
						if (mymap.getelement(x, next.gety() + i).getflag()) {
							rival = mymap.getelement(x, next.gety() + i).getcharacter();
							if (me.getteam().equals(rival.getteam())) {
								rival = null;
								break;
							} else {
								break;
							}
						} else
							i++;
					}
				}
				break;
			case "DOWN":
				while (y - i >= 0) {
					if (mymap.getelement(x, y - i).getflag()) {
						next = mymap.getelement(x, y - i).getcharacter();
						break;
					} else
						i++;
				}
				i = 1;
				if (next != null) {
					while (next.gety() - i >= 0) {
						if (mymap.getelement(x, next.gety() - i).getflag()) {
							rival = mymap.getelement(x, next.gety() - i).getcharacter();
							if (me.getteam().equals(rival.getteam())) {
								rival = null;
								break;
							} else {
								break;
							}
						} else
							i++;
					}
				}
				break;
			}
			if (rival != null && rival.getvisible())
				return rival;
			else
				return null;
		}
	}
}
