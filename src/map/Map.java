package map;

import app.Myapp;

public class Map {
	Element[][] _map= new Element[8][4];

	/**
	 * Constructor 
	 */
	public Map(){
		for (int y=0 ; y < 4 ; y++) {
			for (int x=0; x < 8 ; x++) {
				_map[x][y]= new Element();
			}
		}
	}
	
	/**
	 * Returns the map.
	 */
	public Element[][] getmap(){
		return _map;
	}
	
	/**
	 * This function sets all the element to the map.
	 */
	public void setmap(Element[][] e) {
		_map = e;
		generate();
	}
	
	/**
	 * This function puts the chess on the map randomly. 
	 */
	public void generate() {
		// generate red team
				int count= 0;
		// 
				int xcoor = Myapp.util.ran(0, 7);
				int ycoor = Myapp.util.ran(0, 3);
				if (_map[xcoor][ycoor].getflag() == false) {
					_map[xcoor][ycoor].setcharacter("A",xcoor, ycoor, "Red");
					
				}
				count++;
		// 
				while (count <= 2) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("B",xcoor, ycoor, "Red");
						count++;
					}
				}
		// 
				while (count <= 4) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("C",xcoor, ycoor, "Red");
						count++;
					}
				}
		//
				while (count <= 6) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("D",xcoor, ycoor, "Red");
						count++;
					}
				}
		//
				while (count <= 8) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("E",xcoor, ycoor, "Red");
						
						count++;
					}
				}	
		//
				while (count <= 10) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("F",xcoor, ycoor, "Red");
						count++;
					}
				}	
		//
				while (count <= 15) {
					xcoor = Myapp.util.ran(0, 7);
					ycoor = Myapp.util.ran(0, 3);
					if (_map[xcoor][ycoor].getflag() == false) {
						_map[xcoor][ycoor].setcharacter("G",xcoor, ycoor, "Red");
						count++;
					}
				}	
		// generate black team
				count= 0;
		// 		
				while(count<=0) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("a",xcoor, ycoor, "Black");	
							count++;
						}
				}
		// 
				while (count <= 2) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("b",xcoor, ycoor, "Black");
							count++;
						}
				}
		// 
				while (count <= 4) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("c",xcoor, ycoor, "Black");
							count++;
							}
				}
		//
				while (count <= 6) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("d",xcoor, ycoor, "Black");
							count++;
						}
				}
		//
				while (count <= 8) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("e",xcoor, ycoor, "Black");
							count++;
						}
				}	
		//
				while (count <= 10) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("f",xcoor, ycoor, "Black");
							count++;
						}
				}	
		//
				while (count <= 15) {
						xcoor = Myapp.util.ran(0, 7);
						ycoor = Myapp.util.ran(0, 3);
						if (_map[xcoor][ycoor].getflag() == false) {
							_map[xcoor][ycoor].setcharacter("g",xcoor, ycoor, "Black");
							count++;
						}
				}	
	}
	public Element getelement(int x, int y) {
		Element temp = new Element();
		try {
			temp= _map[x][y];
			}
		catch (ArrayIndexOutOfBoundsException e) {
		}
		return temp;
	}
	
}
