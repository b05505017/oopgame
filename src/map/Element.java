package map;
import java.awt.Color;
import java.awt.Image;
import java.io.FileInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import app.Myapp;
import character.Character;

public class Element extends JButton{
	boolean flag=false;
	Character charac=new Character();
	/**
	 * Constructors
	 */
	public Element() {
		try {
			Image unflipped = ImageIO.read(new FileInputStream("img/unflipped.png"));
			this.setIcon(new ImageIcon(unflipped));
			this.setBackground(new Color(184, 134, 11));
			this.setForeground(new Color(184, 134, 11));
			}
			catch (Exception ex) {
				System.out.println(ex);
			}
		
	}
	public Element(String s) {
		super(s);
	}
	/**
	 * This function sets the icon for the character on this element.
	 */
	public void seticon() {
		this.setIcon(new ImageIcon(Myapp.util.geticon(this.getcharacter().getteam(),this.getcharacter().gettype())));
	}
	
	/**
	 * Returns the character on this element.
	 */
	public Character getcharacter() {
		return charac;
	}
	/**
	 * Sets the character to this element.
	 * @param type
	 * @param x
	 * @param y
	 * @param te
	 */
	public void setcharacter(String type, int x, int y, String te) {
		charac.settype(type);
		charac.setx(x);
		charac.sety(y);
		charac.setteam(te);
		flag=true;	
	}
	
	/**
	 * Returns whether there's a character on this element. 
	 */
	public boolean getflag() {
		return flag;
	}
	/**
	 * Resets the flag if a character has moved away from this element.
	 */
	public void resetflag() {
		flag=false;
		try {
			Image empty = ImageIO.read(new FileInputStream("img/empty.png"));
			this.setIcon(new ImageIcon(empty));
			}
			catch (Exception ex) {
				System.out.println(ex);
			}
	}
	/*
	 * Set the flag if a character moves to this element.
	 */
	public void setflag(Character c) {
		flag = true;
		charac = c ;
		
	}
}

